$('.fotoKota').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:3000,
    autoplayHoverPause:true,
    margin:15,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
            
        },
        600:{
            items:2
           
        },
        1000:{
            items:3
        }
    }
});

$('.owlartikel').owlCarousel({
    loop:true,
    autoplay:true,
    autoplayTimeout:4500,
    autoplayHoverPause:true,
    margin: 4,
    responsiveClass:true,
    responsive:{
        0:{
            items:1
            
        },
        600:{
            items:2
           
        },
        1000:{
            items:3
        }
    }
});
